## Beginning

### What is Architecture anyway? : from Uncle Bob
	Something that has made a Fundamental Impact on me as an Architect
	* Architecture is about Intent (Screaming Architecture) 
	* Separate User Interaction from Delivery Mechanism

* [Architecture: The Lost Years - Uncle Bob](https://www.youtube.com/watch?v=HhNIttd87xs)
* [Another Reference on the same](http://www.codingthearchitecture.com/2011/11/06/the_delivery_mechanism_is_an_annoying_detail.html)
* [A good one-page summary on the same](http://www.codingthearchitecture.com/2011/11/06/the_delivery_mechanism_is_an_annoying_detail.html)
* Tons of videos, articles & books from Uncle Bob
* [Blog on Architecture](https://8thlight.com/blog/tags/architecture.html)


## Design Principles & Patterns
* [SOLID Design Principles](http://butunclebob.com/ArticleS.UncleBob.PrinciplesOfOod)
* [Catalog](https://www.oodesign.com/)
* [Comparison of MVP / MVC / MVVM etc](https://www.codeproject.com/Articles/66585/Comparison-of-Architecture-presentation-patterns-M)

## Exercises
Best Way to develop Architecture Skills is Do - and - Debate

* [Make the Magic go away - Uncle Bob](https://8thlight.com/blog/uncle-bob/2015/08/06/let-the-magic-die.html): The most precious advise to Architects

* [Amazon like system - Code Kata problem](http://codekata.com/kata/kata12-best-sellers/)

* [Rule Engine: Business Rules - Code Kata](http://codekata.com/kata/kata16-business-rules/)

* [Rule Engine: More Business Rules - Code Kata](http://codekata.com/kata/kata17-more-business-rules/)

* [Build your own Web Server](https://ruslanspivak.com/lsbaws-part1/)


## Examples
Learn from Open Source systems, which are known to be really good

* [The Architecture of Open Source Applications](http://aosabook.org/en/index.html) : A classic book to read!

* [Chapter on Git](http://aosabook.org/en/git.html)

* [Design of Git](https://github.com/pluralsight/git-internals-pdf) : Excellent! (thanks to Hemant Shah for sharing)

* [Mingle's serverless Slack app](https://www.thoughtworks.com/mingle/tech/2017/03/26/serverless-slack-app.html)


## Examples: Big Data Architectures
* [Architecture of Giants: Data Stacks at Facebook, Netflix, Airbnb, and Pinterest](https://blog.keen.io/architecture-of-giants-data-stacks-at-facebook-netflix-airbnb-and-pinterest-9b7cd881af54)
* [Evolution of the Netflix Data Pipeline](https://medium.com/netflix-techblog/evolution-of-the-netflix-data-pipeline-da246ca36905)
* [Data Infrastructure at Airbnb](https://medium.com/airbnb-engineering/data-infrastructure-at-airbnb-8adfb34f169c)
* [Apache Samza, LinkedIn’s Framework for Stream Processing](https://thenewstack.io/apache-samza-linkedins-framework-for-stream-processing/): https://thenewstack.io/apache-samza-linkedins-framework-for-stream-processing/
* [Airflow @ Agari](https://www.agari.com/identity-intelligence-blog/airflow-agari/)

## Server-less
* [A good introduction from ThroughtWorks](https://martinfowler.com/bliki/Serverless.html)
* [Detailed Article](https://martinfowler.com/articles/serverless.html)
* [Excellent Talk](https://gojko.net/2017/10/05/serverless-design-gotocph.html)

## Web
* [A good summary of Distributed / Web Architectures](http://berb.github.io/diploma-thesis/original/)

## Microservices
* [From Martin Fowler](https://martinfowler.com/articles/microservices.html)
* [The Reactive Manifesto](https://www.reactivemanifesto.org/glossary#Location-Transparency)
* [Testing Strategies in a Microservice Architecture](https://martinfowler.com/articles/microservice-testing/)
* [Microservice Prerequisites](https://martinfowler.com/bliki/MicroservicePrerequisites.html)

* [The Evolution of Microservices at Scale](https://info.lightbend.com/rs/558-NCX-702/images/ebook-reactive-microservices-the-evolution-of-microservices-at-scale-2.pdf)
* [Microservices to Not Reach Adopt Ring in ThoughtWorks Technology Radar](https://www.infoq.com/news/2018/06/microservices-adopt-radar?utm_campaign=infoq_content&utm_source=infoq&utm_medium=feed&utm_term=global)


## TDD
* [TDD Harms Architecture?](http://blog.cleancoder.com/uncle-bob/2017/03/03/TDD-Harms-Architecture.html)
* [Flexible Design? Testable Design? You Don't Have To Choose!](https://www.youtube.com/watch?v=K3q_y8H1ZTo)